+++
title = "Index"
author = ["Shizuka Star"]
lastmod = 2021-02-28T20:13:44-03:00
tags = ["about"]
draft = false
+++

The **Shizuka Fan Blog** is a blog/wiki dedicated to the Japanese rock band Shizuka, and to its founder Shizuka Miura.
