+++
title = "External Links"
author = ["Shizuka Star"]
lastmod = 2021-02-28T20:13:45-03:00
tags = ["about"]
draft = false
+++

-   Reddit
    -   [r/Shizuka](https://reddit.com/r/shizuka)
    -   [u/cutewaterbottle](https://reddit.com/u/cutewaterbottle)
-   Twitter
    -   [shizukastar](https://twitter.com/shizukastar)
-   Wikipedia
    -   [Shizuka (band)](https://en.wikipedia.org/wiki/Shizuka%5F(band))
    -   [Shizuka discography](https://en.wikipedia.org/wiki/Shizuka%5Fdiscography)
    -   [Shizuka Miura](https://en.wikipedia.org/wiki/Shizuka%5FMiura)
    -   [User:TRANSviada](https://en.wikipedia.org/wiki/User:TRANSviada)
