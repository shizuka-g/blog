+++
title = "2000–2010: From Reunition to End"
author = ["Shizuka Star"]
date = 2020-10-01T05:58:00-03:00
lastmod = 2021-02-28T20:13:49-03:00
categories = ["history"]
draft = false
+++

## 2000--2002: Reunition, Tokyo Underground '95, and the Le Weekend Festival {#2000-2002-reunition-tokyo-underground-95-and-the-le-weekend-festival}

In 2000, the group reunited with the same previous members: Shizuka, Maki, Jun, and Seven. In that year, they released a Shizuka live album which was recorded in 1995 during a 40-minute performance in a Tokyo nightclub, titled "Tokyo Underground 20, Jul '95" ([1](#orgd559eb6)), and published in the United States by the American label Last Visible Dog. As Shizuka's previous albums, _Tokyo Underground '95_[^fn:1] maintains the band's melancholic and contrasting music style, with Shizuka's chant vocals, and Maki's psychedelic guitar solos. Nevertheless, this album has some different characteristics in relation to other Shizuka releases: it starts with a 1-minute recording of a previous live PA; and, as described by Eclipse Records, the "disc gives off a slightly different 'in a nightclub' aura to Shizuka's zoned-out balladry" ([2](#org6814a7e)).

In circa October or November 2001, the band was invited by The Wire magazine to play at the Le Weekend festival, in Stirling, Scotland ([3](#org4c84102)). In this way, Shizuka was scheduled to play at 19:30 on 25 April 2002 at the Tolbooth, a music venue that had just restarted its activities following a £6m refurbishment ([4](#org15b5673)). As the date approached, Shizuka Miura, Maki Miura, Jun Kosugi, and Kazuhide Yamaji flew their way to the United Kingdom. When they arrived at the London airport, Maki had stomach related health issues and was hospitalized in an emergency department at the airport. Shizuka members also faced challenges immigration wise to get into Scotland as they were suspected to be other than musicians who would perform at the Le Weekend. Nevertheless, the band made it to the Tolbooth "with a last-minute appearance", and become the first artist to perform in the 2002 edition of that festival. Gavin Laird, bassist and vocalist of Telstar Ponies, described the experience on the band’s website and wrote about Shizuka challenges and performance ([5](#org8d61cca)).

> I think it’s unlikely that this year I’ll catch a more inspiringly disparate co-headline as Shizuka and the David S Ware Quartet. I’ve been waiting to see Shizuka for something like seven years. So the[ir] appearance on the first night of the festival seems nothing short of miraculous to me. This not least because of the combined forces of some dunderheads at immigration who wouldn’t believe they were who they said they were when entering the country and a stomach complaint striking guitarist Maki Miura. Indeed, Miura hypes the pre-gig nerves for everyone with a last-minute appearance at the venue and a seemingly interminable rearrangement of his effects pedals before he’s ready to play.
>
> When they do finally launch into their set they look scared as hell but produce exactly the beautiful spindly psychedelic sound I’d anticipated. Miura is one of my all-time guitar heroes (ha! can you tell?) and he doesn’t disappoint me. He plays beautiful simple lines but fills them with an emotional intensity that puts the lard brained mundanity of almost all currently lauded guitar music to shame.
> ---Gavin Laird


## 2008: Traditional Aesthetics {#2008-traditional-aesthetics}

On 25 April 2008, a Shizuka live performance recording from September 1995 at Namba Bears was released in Japan by PSF Records: the live album _Traditional Aesthetics_ (伝承美学, _Denshō Bigaku_) ([6](#orga10a2f3), [7](#org87ae4a1)). As characteristics of Shizuka's musical style, the songs in Traditional Aesthetics are simple, going from a slow rhythm guided by Shizuka plaintive and chant-styled, and out of tune, vocals to crescendos featuring Maki's psychedelic guitar solos. Notably, the album themes are elegies, negative emotions or subjects like loneliness and hell, and nature elements ([8](#orgf6c87e1)). Reviewing Traditional Aesthetics, Bill Meyer wrote on The Wire magazine ([8](#orgf6c87e1)):

> Shizuka's songs are certainly simplistic, and she's hardly the most gifted of performers, but in alliance with her musicians—including guitarist Maki Miura and drummer Jun Kosugi (both of Fushitsusha fame)—something of real and rare intensity is conjured.
>
> She delivers her fragile elegies in a haunted bellow, flirting with tunelessness but possessed of the same monomaniacal insistence on her own emotional dislocation as Nico. She sings "A blinding light suddenly shines out/Brighter than any star" in exactly the same tone as "I will surely go to hell/Friendless and alone". Over its 15-minute duration, "Heavenly Persona" keeps drifting loose and then mustering itself for another slow-motion assault, segueing gradually into "A Song For The World Left Behind", which, impressively, sounds as devastatingly forlorn as its title.
>
> Maki Miura, meanwhile, runs through the well-worn catalogue of psych guitar pyrotechnics, yet somehow his echo-drenched waves of distortion sound invigoratingly fresh.
> ---Bill Meyer

Members in this recording are Shizuka (vocals and guitar), Maki (lead
guitar), Jun (drums), and Seven (bass)
([6](#orga10a2f3)).


## 2008--2010: Shizuka Miura's Death and The End of Shizuka {#2008-2010-shizuka-miura-s-death-and-the-end-of-shizuka}

On circa 31 January 2010, Shizuka Miura died, marking the end of her group. As a tribute to Shizuka, a concert of the band performing at ShowBoat in December 2008 was released on the DVD-video format by PSF Records on 25 April 2010: _Endless Dream_ (_Owari no Nai Yume_). Describing the album, the PSF website published ([9](#orga5e8508)):

> As ever, the focus is upon Shizuka's delicate, uncanny vocals and tremulous sense of timing which meld with the overwhelmingly emotive white-hot nebulae of ex-Fushitsusha axeman Maki Miura's guitar. Together the narcotic voice and the guitar peak and churn in perfectly weightless filigreed forms of light, darkness, cosmic cold and invisible motion. At their best, the Shizuka group were one of the distinctively original voices in the Japanese psych scene.

Members in _Owari no Nai Yume_ are Shizuka (vocals, guitar), Maki (lead guitar), Kazuhide Yamaji (bass), and Katsumi Honjo (drums) ([9](#orga5e8508)).


## Posthumous Releases {#posthumous-releases}

On 24 May 2017, a live recording from March 2009 at Kameido Hardcore of the song "Lunatic Peal" (気の真珠, _Ki no Shinju_) was included on a various artists compilation, _Tokyo Flashback: P.S.F. — Psychedelic Speed Freaks_, by Super Fuji Discs; the compilation was also later reissued by Black Editions records in 2019 ([10](#org86ca871)).

In early 2018, it was announced that the American label Black Editions will reissue a remastered version of _Heavenly Persona_. On the Bandcamp Daily, it was said that the audio engineer Kris Lapke was "enlisted to remaster" the reissue ([11](#org5ee3bdf)).

Since 2019, a new Shizuka live album, titled _Paradise of Delusion_ (妄想の 楽園, _Mousou no Rakuen_), has been announced to be released by the French label An’archives. This pre-announced album, to be released on the double-sided vinyl format, has a total of 8 tracks taken from a previously unreleased live recording in 2001 ([12](#org6ed66c7)).


## References {#references}

<a id="orgd559eb6"></a>1.  MUGISHIMA, Tetsuya "Seven" and RECORDS, Last Visible Dog. _Shizuka - Tokyo Underground 20, Jul ’95_ [online]. 2000. Last Visible Dog Records. Available from: <https://web.archive.org/web/20180915225519/http://lvd.4mg.com/catalog/lvd030.htm>

<a id="org6814a7e"></a>2.  RECORDS, Aquarius and RECORDS, Eclipse. _Shizuka - Tokyo Underground ’95_ [online]. 2001. Last Visible Dog Records. Available from: <http://lvd.4mg.com/030.htm>

<a id="org4c84102"></a>3.  HUANG, Evelyn "ethereal". _最接近天國的異界之音──靜香_ [online]. 2001. Rocker. Final. Available from: <https://jerrylovesamuel.pixnet.net/blog/post/158334374>

<a id="org15b5673"></a>4.  PIE, Poison. _Le Weekend: Back at the Tolbooth_ [online]. 2002. Poison Pie Publishing House. Available from: <https://poisonpie.com/sounds/haino/text/leweekend%5F02.html>

<a id="org8d61cca"></a>5.  LAIRD, Gavin. _Le Weekend, Stirling, 25th–28th April 2002: Day One_ [online]. 2002. Telstar Ponies. Available from: <https://web.archive.org/web/20030212115630/http://www.telstarponies.com/lwone.htm>

<a id="orga10a2f3"></a>6.  RECORDS, PSF. _PSFD-178: Shizuka / Live - Traditional Aesthetics_ [online]. 2008. PSF Records. Available from: <https://web.archive.org/web/20160407103425/http://psfrecords.com/list161-180.html>

<a id="org87ae4a1"></a>7.  RECORDS, PSF. _静香 / 伝承美学 [PSFD-178]_ [online]. 2008. PSF Records. Available from: <https://web.archive.org/web/20090204121105/http://www.psfmm.com/product/10957>

<a id="orgf6c87e1"></a>8.  MEYER, Bill. _Shizuka - Traditional Aesthetics_ [online]. 2008. The Wire. 294. Available from: <https://www.thewire.co.uk/issues/294>

<a id="orga5e8508"></a>9.  RECORDS, PSF. _Shizuka / Owari no Nai Yume (DVD)_ [online]. 2010. PSF Records. Available from: <https://web.archive.org/web/20160407120135/http://psfrecords.com/listLPsingle.html>

<a id="org86ca871"></a>10.  DALE, Jon. _Tokyo Flashback: An essential guide to Japanese label PSF Records_ [online]. 2019. The Vinyl Factory. Available from: <https://thevinylfactory.com/features/essential-guide-psf-records>

<a id="org5ee3bdf"></a>11.  REYES, Jordan. _Black Editions Keeps P.S.F. Records’ Experimental Spirit Alive_ [online]. 2018. Bandcamp. Available from: <https://daily.bandcamp.com/label-profile/black-editions-label-profile>

<a id="org6ed66c7"></a>12.  AN’ARCHIVES. _Shizuka: 妄想の楽園 | Paradise of Delusion_ [online]. 2019. Low Company. Available from: <https://web.archive.org/web/20200519030338/https://www.lowcompany.co.uk/products/paradise-of-delusion>

[^fn:1]: This album was reissued by the same label in circa 2003 as "Tokyo Underground '95" <sup id="4fafe9b3c531fad8ba9c4e9fb6fae076"><a href="#aquariusrecords-2001-shizuka-tokyoundergro95" title="@misc{aquariusrecords-2001-shizuka-tokyoundergro95, author = {{Aquarius Records} and {Eclipse Records}}, publisher = {Last Visible Dog Records}, title = {{Shizuka - Tokyo Underground '95}}, url = {http://lvd.4mg.com/030.htm}, year = {2001} }">aquariusrecords-2001-shizuka-tokyoundergro95</a></sup>.
