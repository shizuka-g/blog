+++
title = "International Presentations"
author = ["Shizuka Star"]
date = 2020-09-05T04:53:00-03:00
lastmod = 2021-02-28T20:13:49-03:00
tags = ["tour", "united-states", "san-francisco", "kilowatt", "los-angeles", "scotland", "stirling", "tolbooth", "festival", "shizuka-miura", "maki-miura", "jun-kosugi", "tetsuya-seven-mugishima", "kazuhide-yamaji", "mason-jones", "lindsay", "alan-cummings", "health-issue"]
categories = ["history", "presentation", "international"]
draft = false
[header]
  image = "/img/cache_060a131e-1ca1-4ef7-ceed-6f4b2ec26d68.ed2.png"
  description = "The rare international performances of Shizuka in the USA and in Scotland."
+++

## Introduction {#introduction}

Shizuka, as a musical project that emerged from the Tokyo underground psychedelic scene of the 1990s, relied on live performances as an important platform for the dissemination of its music. It's no coincidence that, throughout its 18 years of existence ([1](#org804b043)), the band played many shows in their homeland, some of which in turn came to be released as live albums—Shizuka's major release format. However, the group faced some challenges ([2](#org6535298)) that prevented them from extensively touring overseas, being monetary and health issues two of the main reasons as described by Mason Jones ([2](#org6535298)). Nevertheless, Shizuka was able to tour in the United States twice ([3](#org88751f5)) and to also play in a festival hosted in Scotland ([4](#org32de66d)).

{{< figure src="/blog/image/8e5f3c5b-555e-4248-9232-19f81eae77fc.png" >}}


## USA Tours {#usa-tours}

Shizuka arguably had its most prolific period in the mid-1990s. It was when they recorded most of their live albums, being the first one, _Live Shizuka_, released in 1995 in the United States ([2](#org6535298), [5](#orgecccff7), [6](#org1c8698f)). At that time, Shizuka, with members Shizuka Miura and Maki Miura, went twice to the US for touring purposes ([3](#org88751f5)), playing mainly or exclusively in the state of California ([7](#org1cb9c81)).

Tetsuya “Seven” Mugishima, who was Shizuka's bassist at that time ([8](#org05b390a), [9](#org341c02f)), was behind the early organization of the tours, although, like Jun Kosugi, he didn't go to the US. For the first tour, Seven contacted two key people through e-mail: Lindsay (possibly an event organizer. Lindsay was based in Boston) ([10](#org12145c4)), and David Newgarden (a CMJ staffer at the time, who wrote for the CMJ New Music Report). This e-mailing stopped, though, after Tetsuya left the band for the first time in late 1995 or early 1996. In 1997, Seven restarted the e-mailing and told that Lindsay might have been the person who formed the second tour ([11](#orgc3b1e35)).

Shizuka performed in San Francisco on both occasions ([12](#org9256db0)), being the American software engineer and musician Mason Jones the organizer for the shows in that city ([3](#org88751f5), [9](#org341c02f)). There, they made 2 shows, one of which was at Kilowatt (a dive bar in which other Japanese underground artists, like Angel'in Heavy Syrup and Melt-Banana, also played at in the 1990s), on 22 June 1997 ([3](#org88751f5)). They also went twice to a local radio station and played a live show on the radio during one of these visits. Shizuka and Maki remembered having a good time with many fans, and that the audience was quite receptive in SF, who danced to their music ([9](#org341c02f)).

{{< figure src="/blog/image/e596336c-8070-420b-ad91-922f78b27051.jpg" caption="Figure 1: Kilowatt home's building on 25 January 2012." >}}

Shizuka also got to play in Los Angeles ([9](#org341c02f)), but the details remain uncertain. Mason Jones said that it might have been at the former nightclub Spaceland, but didn't remember who could've been the organizer ([13](#orgc35fa66)) in a time that “LA contacts for non-noise shows were harder to come by” ([10](#org12145c4)).

Since only Shizuka and Maki went from the group's core formation ([9](#org341c02f)), Junko Hiroshige played the bass for the band during one of the visits, which had some “tensions”, Mason Jones records ([7](#org1cb9c81)). This coincides with Junko being the bassist in the recording “Kimi no Sora”, a track on the compilation album _Land of Raising Noise, Vol. 2_, released by Jones' Charnel Music in April 1997.

And then Shizuka sort of entered a hiatus ([9](#org341c02f)).


## Le Weekend Festival in Scotland {#le-weekend-festival-in-scotland}

{{< figure src="/blog/image/1915bf21-bf01-43e5-be41-a921b0946bf1.jpg" caption="Figure 2: The Tolbooth, a venue of art and music in Stirling, Scotland." >}}

In 2000, with the reunition of the band and release of their second live album, “Tokyo Underground \`95” ([9](#org341c02f), [14](#org94559e9)), Shizuka returned to the stage. In circa October or November 2001, the group was invited by The Wire magazine to play at the Le Weekend festival, in Stirling, Scotland ([9](#org341c02f)). Seven, who had rejoined the band, also initially e-mailed a person involved in the organization of this event, Alan Cummings ([15](#orgc13397f)), the British music journalist that specializes in the Japanese underground scene ([16](#orgb36e38f)). In this way, Shizuka had been scheduled to play at 19:30 on 25 April 2002, at the Tolbooth, a music venue that had just restarted its activities following a £6m refurbishment ([17](#orgafc5cd9)). One week before the scheduled date, Maki fired Seven once again from the band, and Kazuhide Yamaji joined in his place, Seven related ([15](#orgc13397f), [18](#org7628271)).

As the date approached, Shizuka Miura, Maki Miura, Jun Kosugi, and Kazuhide Yamaji flew their way to the United Kingdom. When they arrived at the London airport, Maki had stomach related health issues and was hospitalized in an emergency department at the airport. At this moment, Yamaji called Seven informing the situation, and then Seven contacted Cummings for help ([19](#org01663c8)). Shizuka members also faced challenges immigration wise to get into Scotland, as they were suspected (probably by authorities) to be other than musicians who would play at the Le Weekend ([20](#org6ed94fd)). Nevertheless, the band made it to the Tolbooth ([4](#org32de66d)) "with a last minute appearance", and become the first artist to perform in the 2002 edition of that festival ([20](#org6ed94fd)).

<a id="org8ba8832"></a>

{{< figure src="/blog/image/c296c6c8-68e2-4540-a3f5-3131f7f1e373.png" caption="Figure 3: Shizuka members playing at the Le Weekend festival. From left to right: Kazuhide Yamaji, Shizuka Miura, Jun Kosugi, Maki Miura. Photography by Sarah Goodwin." >}}

Members of the Scottish post-rock band [Telstar Ponies](https://en.wikipedia.org/wiki/Telstar%5FPonies) were involved in the organization of this festival. Particularly, [David Keenan](https://en.wikipedia.org/wiki/David%5FKeenan) was the curator for the Le Weekend 2002 ([21](#org6f81ec6)). Also, [Gavin Laird](https://twitter.com/i/user/236446985), bassist and vocalist of Telstar Ponies, described the experience on the [band's official website](https://web.archive.org/web/20030603172100/http://www.telstarponies.com:80/index.htm) and wrote about Shizuka challenges and performance ([20](#org6ed94fd)).

> I think it's unlikely that this year I'll catch a more inspiringly disparate co-headline as Shizuka and the David S Ware Quartet. I've been waiting to see Shizuka for something like seven years. So the[ir] appearance on the first night of the festival seems nothing short of miraculous to me. This not least because of the combined forces of some dunderheads at immigration who wouldn't believe they were who they said they were when entering the country and a stomach complaint striking guitarist Maki Miura. Indeed Miura hypes the pre-gig nerves for everyone with a last minute appearance at the venue and a seemingly interminable rearrangement of his effects pedals before he's ready to play.
>
> When they do finally launch into their set they look scared as hell but produce exactly the beautiful spindly psychedelic sound I'd anticipated. Miura is one of my all time guitar heroes (ha! can you tell?) and he doesn't disappoint me. He plays beautiful simple lines but fills them with an emotional intensity that puts the lard brained mundanity of almost all currently lauded guitar music to shame.
>
> -- Gavin Laird

When it comes to video footage, Seven said he assumes that a recording of Shizuka's performance at the Tolbooth was never made ([19](#org01663c8)) since the stomach pain Maki was feeling would have made him look uncomfortable ([4](#org32de66d)). Still, two of three Shizuka photos (fig. [3](#org8ba8832), fig. [4](#orga3c2576)) by Sarah Goodwin were retrieved from the former Telstar Ponies website, which was possible because it is archived on the Wayback Machine ([20](#org6ed94fd)). These images were processed by [Waifu2x](https://en.wikipedia.org/wiki/Waifu2x), a software for image scaling and noise reduction.

<a id="orga3c2576"></a>

{{< figure src="/blog/image/e334ca4d-c20e-49a2-a9c4-a0aa60c09513.png" caption="Figure 4: Shizuka Miura (and possibly Jun Kosugi in the background) playing at the Le Weekend festival of 2002, at the Tolbooth. Photography by Sarah Goodwin." >}}


## Thanks {#thanks}

Special thanks to [Mason Jones](https://twitter.com/i/user/23475752) and [Tetsuya “Seven” Mugishima](https://twitter.com/i/user/97904702).


## References {#references}

<a id="org804b043"></a>1.  CUMMINGS, Alan. _Shizuka Miura_ [online]. 2010. The Wire. Available from: <https://www.thewire.co.uk/news/18497/shizuka-miura>

<a id="org6535298"></a>2.  JONES, Mason. _Shizuka R.I.P._ [online]. 2010. Ongakublog. Available from: <https://ongakublog.wordpress.com/2010/02/15/shizuka-r-i-p>

<a id="org88751f5"></a>3.  JONES, Mason. _Status nº 1297695011836854272_ [online]. 2020. Twitter. Available from: <https://twitter.com/23475752/status/1297695011836854272>

<a id="org32de66d"></a>4.  MUGISHIMA, Tetsuya "Seven". _Status nº 1299621916039233537_ [online]. 2020. Twitter. Available from: <https://twitter.com/97904702/status/1299621916039233537>

<a id="orgecccff7"></a>5.  MOED, Andrea. _Shizuka - Live Shizuka_ [online]. 1995. CMJ. 28. Available from: <https://books.google.com/books?id=%5FSwEAAAAMBAJ&pg=PT17>

<a id="org1c8698f"></a>6.  UNKOWN. _Shizuka - Live Shizuka_ [online]. 1996. Options Network. 66-71. Available from: <https://books.google.com/books?id=iC9LAAAAYAAJ>

<a id="org1cb9c81"></a>7.  JONES, Mason. _Status nº 1298860549795528704_ [online]. 2020. Twitter. Available from: <https://twitter.com/23475752/status/1298860549795528704>

<a id="org05b390a"></a>8.  MUGISHIMA, Tetsuya "Seven" and RECORDS, Last Visible Dog. _Shizuka - Tokyo Underground 20, Jul ’95_ [online]. 2000. Last Visible Dog Records. Available from: <https://web.archive.org/web/20180915225519/http://lvd.4mg.com/catalog/lvd030.htm>

<a id="org341c02f"></a>9.  HUANG, Evelyn "ethereal". _最接近天國的異界之音──靜香_ [online]. 2001. Rocker. Final. Available from: <https://jerrylovesamuel.pixnet.net/blog/post/158334374>

<a id="org12145c4"></a>10.  JONES, Mason. _Status nº 1298772407163871232_ [online]. 2020. Twitter. Available from: <https://twitter.com/23475752/status/1298772407163871232>

<a id="orgc3b1e35"></a>11.  MUGISHIMA, Tetsuya "Seven". _Status nº 1298889986691874816_ [online]. 2020. Twitter. Available from: <https://twitter.com/97904702/status/1298889986691874816>

<a id="org9256db0"></a>12.  JONES, Mason. _Status nº 1297336354938843136_ [online]. 2020. Twitter. Available from: <https://twitter.com/23475752/status/1297336354938843136>

<a id="orgc35fa66"></a>13.  JONES, Mason. _Status nº 1298491888652005378_ [online]. 2020. Twitter. Available from: <https://twitter.com/23475752/status/1298491888652005378>

<a id="org94559e9"></a>14.  RECORDS, Aquarius and RECORDS, Eclipse. _Shizuka - Tokyo Underground ’95_ [online]. 2001. Last Visible Dog Records. Available from: <http://lvd.4mg.com/030.htm>

<a id="orgc13397f"></a>15.  MUGISHIMA, Tetsuya "Seven". _Status nº 1298795074055102465_ [online]. 2020. Twitter. Available from: <https://twitter.com/97904702/status/1298795074055102465>

<a id="orgb36e38f"></a>16.  PSYCHMETALFREAK. _An Interview with Japanese Underground Guru Alan Cummings_ [online]. 2012. PsychMetalFreak. Available from: <https://psychmetalfreak.blogspot.com/2012/06/interview-with-top-japanese-underground.html>

<a id="orgafc5cd9"></a>17.  PIE, Poison. _Le Weekend: Back at the Tolbooth_ [online]. 2002. Poison Pie Publishing House. Available from: <https://poisonpie.com/sounds/haino/text/leweekend%5F02.html>

<a id="org7628271"></a>18.  MUGISHIMA, Tetsuya "Seven". _Status nº 1298824273331499011_ [online]. 2020. Twitter. Available from: <https://twitter.com/97904702/status/1298824273331499011>

<a id="org01663c8"></a>19.  MUGISHIMA, Tetsuya "Seven". _Status nº 1298835997342314501_ [online]. 2020. Twitter. Available from: <https://twitter.com/97904702/status/1298835997342314501>

<a id="org6ed94fd"></a>20.  LAIRD, Gavin. _Le Weekend, Stirling, 25th–28th April 2002: Day One_ [online]. 2002. Telstar Ponies. Available from: <https://web.archive.org/web/20030212115630/http://www.telstarponies.com/lwone.htm>

<a id="org6f81ec6"></a>21.  LAIRD, Gavin. _Le Weekend, Stirling, 25th–28th April 2002: Day Four_ [online]. 2002. Telstar Ponies. Available from: <https://web.archive.org/web/20030324043144/http://www.telstarponies.com/lwfour.htm>
