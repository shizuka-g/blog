+++
title = "Lyrics"
author = ["Shizuka Star"]
date = 2021-02-28T18:24:00-03:00
lastmod = 2021-02-28T20:13:45-03:00
tags = ["lyrics"]
categories = ["archive", "lyrics"]
draft = false
+++

## 6g Star (6gの星, 6g no Hoshi) {#6g-star--6gの星-6g-no-hoshi}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  6gの星 [Official from Traditional Aesthetics]

    6グラムの星を下さい<br />
    私はそれをかき混ぜる<br />
    螺旋階段を降りるように<br />
    星をぐるぐるかき混ぜる<br />

    私はきっと地獄へ落ちる<br />
    もう誰とも会えない<br />

    6グラムの星を下さい<br />
    私はそれをかき混ぜる<br />
    螺旋階段を昇るように<br />
    星をぐるぐるかき混ぜる<br />

    このまま昇ってゆけば<br />
    空へ行ったあの人に会える<br />


#### Kana {#kana}

<!--list-separator-->

-  6g の ほし

    6 グラム の ほし を ください<br />
    わたくし は それ を かきまぜる<br />
    らせん かいだん を おりる よう に<br />
    ほし を ぐるぐる かきまぜる<br />

    わたくし は きっと じごく へ おちる<br />
    もう だれ と も あえない<br />

    6 グラム の ほし を ください<br />
    わたくし は それ を かきまぜる<br />
    らせん かいだん を のぼる よう に<br />
    ほし を ぐるぐる かきまぜる<br />

    この まま のぼって ゆけ ば<br />
    そら へ いった あの ひと に あえる<br />


#### Romaji {#romaji}

<!--list-separator-->

-  6g no hoshi

    6 (roku) guramu no hoshi o kudasai<br />
    watashi wa sore o kakimazeru<br />
    rasen kaidan o oriru yō ni<br />
    hoshi o guruguru kakimazeru<br />

    watashi wa kitto jigoku e ochiru<br />
    mō dare to mo aenai<br />

    6 (roku) guramu no hoshi o kudasai<br />
    watashi wa sore o kakimazeru<br />
    rasen kaidan o noboru yō ni<br />
    hoshi o guruguru kakimazeru<br />

    kono mama nobotte yuke ba<br />
    sora e itta ano hito ni aeru<br />


### English {#english}

<!--list-separator-->

-  6g Star [Official from Traditional Aesthetics]

    Give me a 6g star<br />
    Then let me stir it in circles<br />
    Like descending spiral stairs<br />
    I'll stir the stars round and round<br />

    I will surely go to hell<br />
    Friendless and alone<br />

    Give me a 6g star<br />
    Then let me stir it in circles<br />
    Like climbing spiral stairs<br />
    I'll stir the stars round and round<br />

    If I could only ascend at this moment<br />
    I could meet him in heaven<br />


## A Song For the World Left Behind (世に残す歌, Yo Ni Nokosu Uta) {#a-song-for-the-world-left-behind--世に残す歌-yo-ni-nokosu-uta}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  世に残す歌 [Official from Traditional Aesthetics]

    きみたみのおしえのこして<br />
    あにかえるとてないためそ<br />
    わがみたまひとはあものも<br />
    うえにあるわれはかんむり<br />
    かえしのとうた<br />

    ひとつねにかみにむかわば<br />
    よのみみにあかはもとの<br />
    さいしかにきよめたまいて<br />
    さごくしのふゆのかがみに<br />
    いるとおもえば<br />


#### Kana {#kana}

<!--list-separator-->

-  よにのこすうた

    きみ たみ の おしえ の こして<br />
    あ に かえる とて ない ため そ<br />
    わが みたま ひと は あ もの も<br />
    うえ に ある われ は かんむり<br />
    かえし の とうた<br />

    ひと つね に かみ に むかわ ば<br />
    よ の みみ に あか は もと の<br />
    さい しか に きよめたまいて<br />
    さ ご くし の ふゆ の かがみ に<br />
    いる と おもえ ば<br />


#### Romaji {#romaji}

<!--list-separator-->

-  Yo Ni Nokosu Uta

    kimi tami no oshie no koshite<br />
    a ni kaeru tote nai tame so<br />
    waga mitama hito wa a mono mo<br />
    ue ni aru ware wa kanmuri<br />
    kaeshi no tōta<br />

    hito tsune ni kami ni mukawa ba<br />
    yo no mimi ni aka wa moto no<br />
    sai shika ni kiyometamaite<br />
    sa go kushi no fuyu no kagami ni<br />
    iru to omoe ba<br />


### English {#english}


#### A Song For the World Left Behind [Official from Traditional Aesthetics] {#a-song-for-the-world-left-behind-official-from-traditional-aesthetics}

Leaving behind laws for the monarch and people<br />
Now I return to heaven ? do not grieve for me<br />
My soul now belongs to heaven<br />
Now I reside above you, become a crown<br />


## Awanorata (アワノラタ) {#awanorata--アワノラタ}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  アワノラタ [Official from Live Shizuka]

    アカハナマ<br />
    イキヒニミウク<br />
    フヌムエケ<br />
    ヘネメオコホノ<br />
    モトロソヨ<br />
    ヲテレセヱツル<br />
    スユンチリ<br />
    シヰタラサヤワ<br />


#### Hiragana {#hiragana}

<!--list-separator-->

-  あわのらた [Unofficial from Hotsuma Tsutae]

    あかはなま<br />
    いきひにみうく<br />
    ふぬむえけ<br />
    へねめおこほの<br />
    もとろそよ<br />
    をてれせゑつる<br />
    すゆんちり<br />
    しゐたらさやわ<br />


#### Romaji {#romaji}

<!--list-separator-->

-  Awanorata [Unofficial from Hotsuma Tsutae]

    a ka ha na ma<br />
    i ki hi ni mi u ku<br />
    hu nu mu e ke<br />
    he ne me o ko ho no<br />
    mo to ro so yo<br />
    wo te re se ye tu ru<br />
    su yu wn ti ri<br />
    si yi ta ra sa ya wa<br />


## Bewitchment Floats in Ochre Shadows (オークルの影に浮かぶ幻惑) {#bewitchment-floats-in-ochre-shadows--オークルの影に浮かぶ幻惑}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  オークルの影に浮かぶ幻惑 [Official from Traditional Aesthetics]

    花びらを千切る小さく千切る<br />
    風がさらって行く<br />
    壁にもたれてそれを見ている<br />
    空に解けてしまう<br />

    あなたがくれたやさしさは「空」<br />
    悲しみが風に舞い空に溶けてしまう<br />


#### Kana {#kana}

<!--list-separator-->

-  オークル の かげ に うかぶ げんわく

    はなびら を ちぎる ちいさく ちぎる<br />
    かぜ が さらって いく<br />
    かべ に もたれて それ を みて いる<br />
    そら に とけて しまう<br />

    あなた が くれた やさし-さ は「 そら」<br />
    かなしみ が ふう に まい そら に とけて しまう<br />


#### Romaji {#romaji}

<!--list-separator-->

-  ōkuru no kage ni ukabu genwaku

    hanabira o chigiru chīsaku chigiru<br />
    kaze ga saratte iku<br />
    kabe ni motarete sore o mite iru<br />
    sora ni tokete shimau<br />

    anata ga kureta yasashi-sa wa‘ sora’<br />
    kanashimi ga fū ni mai sora ni tokete shimau<br />


### English {#english}


#### Bewitchment Floats in Ochre Shadows [Official from Traditional Aesthetics] {#bewitchment-floats-in-ochre-shadows-official-from-traditional-aesthetics}

Slicing flower petals, slicing them small<br />
The wind snatches them away<br />
Leaning against a wall, staring at it<br />
Unravelled by the sky<br />

That gentleness that led you astray was a lie<br />
Sorrow dancing on the wind, melting into the sky<br />


## Flowers for the End {#flowers-for-the-end}


### English {#english}


#### Flowers for the End [Official from Live Shizuka] {#flowers-for-the-end-official-from-live-shizuka}

The flowers that I've been growing<br />
in my body for the end look beautiful.<br />
Opening the door to your closed heart,<br />
my piercing wounds magnificently<br />
become yet graver.<br />


### Portuguese {#portuguese}


#### Flores para o Fim [Unofficial By Shizuka Star from Official English on Live Shizuka] {#flores-para-o-fim-unofficial-by-shizuka-star-from-official-english-on-live-shizuka}

As flores para o fim que eu tenho cultivado<br />
no meu corpo estão lindas.<br />
Ao abrir a porta do seu coração que havia se trancafiado,<br />
minhas doloridas feridas<br />
tornam-se, magnificantemente, ainda mais graves.<br />


## Glass Ribbon Unraveled {#glass-ribbon-unraveled}


### English {#english}


#### Glass Ribbon Unraveled [Official from Live Shizuka] {#glass-ribbon-unraveled-official-from-live-shizuka}

Glass ribbon unravels limitlessly,<br />
clearest eye could shoot the sky.<br />
For instance, like an even-tempered nymph.<br />
Try and lick our wounded wrist just a little.<br />
The evening when you lost something precious,<br />
take it with you far away.<br />
Something that was sparkling that playful evening,<br />
take them with you far away.<br />


### Portuguese {#portuguese}


#### Fita de Vidro Desenrolada [Unofficial by Shisuka Star from Official English on Live Shizuka] {#fita-de-vidro-desenrolada-unofficial-by-shisuka-star-from-official-english-on-live-shizuka}

A fita de vidro se desenrola infinitamente,<br />
[tanto que] o mais límpido dos olhos conseguiria fotagrafar o céu.<br />
Como uma serena ninfa,<br />
tente lamber nosso pulso machucado só um pouquinho.<br />
A noite em que você perdeu algo precioso.<br />
Levê-o consigo para longe daqui!<br />
Algo que reluzia naquela noite tão divertida.<br />
Levê-os consigo para longe daqui!<br />


## Heavenly Persona (天界のペルソナ, Tenkai no Perusona) {#heavenly-persona--天界のペルソナ-tenkai-no-perusona}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  天界のペルソナ [Official from Traditional Aesthetics]

    呪われたこの地をやがて太陽よりも<br />
    眩しい光が<br />
    闇を引き裂き闇を従えて<br />
    内なる声が結ばれる<br />


#### Romaji {#romaji}

<!--list-separator-->

-  Tenkai no Peusona [Unofficial by (j-talk.com AND Shizuka Star) from Offical from Traditional Aesthetics]

    norowareta kono chi o yagate taiyō yori mo<br />
    mabushī hikari ga<br />
    yami o hikisaki yami o shitagaete<br />
    uchi naru koe ga musubareru<br />


### English {#english}


#### Heavenly Persona [Official from Live Shizuka] {#heavenly-persona-official-from-live-shizuka}

At least, a blinding light from the sun<br />
has torn out an opening,<br />
conquered an opening<br />
in this cursed land...<br />
Tied up with it an inner voice...<br />


## Heavenly Persona / A Song For the World Left Behind (天界のペルソナ〜世に残す歌, Tenkai no Perusona / Yo Ni Nokosu Uta) {#heavenly-persona-a-song-for-the-world-left-behind--天界のペルソナ-世に残す歌-tenkai-no-perusona-yo-ni-nokosu-uta}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  天界のペルソナ〜世に残す歌 [Official from Traditional Aesthetics]

    <!--list-separator-->

    -  天界のペルソナ

        呪われたこの地をやがて太陽よりも<br />
        眩しい光が<br />
        闇を引き裂き闇を従えて<br />
        内なる声が結ばれる<br />

    <!--list-separator-->

    -  世に残す歌

        きみたみのおしえのこして<br />
        あにかえるとてないためそ<br />
        わがみたまひとはあものも<br />
        うえにあるわれはかんむり<br />
        かえしのとうた<br />

        ひとつねにかみにむかわば<br />
        よのみみにあかはもとの<br />
        さいしかにきよめたまいて<br />
        さごくしのふゆのかがみに<br />
        いるとおもえば<br />


#### Kana {#kana}

<!--list-separator-->

-  てんかいのペルソナ〜よにのこすうた [Unofficial by (J-talk.com AND Shizuka Star) from Official Japanese from Traditional Aesthetics]

    <!--list-separator-->

    -  てんかいのペルソナ

        のろわれた この ち を やがて たいよう より も<br />
        まぶしい ひかり が<br />
        やみ を ひきさき やみ を したがえて<br />
        うち なる こえ が むすばれる<br />

    <!--list-separator-->

    -  よにのこすうた

        きみ たみ の おしえ の こして<br />
        あ に かえる とて ない ため そ<br />
        わが みたま ひと は あ もの も<br />
        うえ に ある われ は かんむり<br />
        かえし の とうた<br />

        ひと つね に かみ に むかわ ば<br />
        よ の みみ に あか は もと の<br />
        さい しか に きよめたまいて<br />
        さ ご くし の ふゆ の かがみ に<br />
        いる と おもえ ば<br />


#### Romaji [Unofficial by (J-talk.com AND Shizuka Star) from Official Japanese from Traditional Aesthetics] {#romaji-unofficial-by--j-talk-dot-com-and-shizuka-star--from-official-japanese-from-traditional-aesthetics}

<!--list-separator-->

-  Tenkai no Perusona / Yo Ni Nokosu Uta

    <!--list-separator-->

    -  Tenkai no Perusona

        norowareta kono chi o yagate taiyō yori mo<br />
        mabushī hikari ga<br />
        yami o hikisaki yami o shitagaete<br />
        uchi naru koe ga musubareru<br />

    <!--list-separator-->

    -  Yo Ni Nokosu Uta

        kimi tami no oshie no koshite<br />
        a ni kaeru tote nai tame so<br />
        waga mitama hito wa a mono mo<br />
        ue ni aru ware wa kanmuri<br />
        kaeshi no tōta<br />

        hito tsune ni kami ni mukawa ba<br />
        yo no mimi ni aka wa moto no<br />
        sai shika ni kiyometamaite<br />
        sa go kushi no fuyu no kagami ni<br />
        iru to omoe ba<br />


### English {#english}


#### Heavenly Persona / A Song For the World Left Behind [Official from Traditional Aesthetics] {#heavenly-persona-a-song-for-the-world-left-behind-official-from-traditional-aesthetics}

<!--list-separator-->

-  Heavenly Persona

    Over a cursed land, a blinding light suddenly shines out<br />
    Brighter than any sun<br />
    Darkness rent, subjugated<br />
    An inner voice made whole<br />

<!--list-separator-->

-  A Song For the World Left Behind

    Leaving behind laws for the monarch and people<br />
    Now I return to heaven ? do not grieve for me<br />
    My soul now belongs to heaven<br />
    Now I reside above you, become a crown<br />


## Planning for Loneliness (孤独を図る, Kodoku o Hakaru) {#planning-for-loneliness--孤独を図る-kodoku-o-hakaru}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  孤独を図る [Unofficial by Evelyn Huang and Hitomi Cecily from Heavenly Persona]

    私の傷口に流れ込む<br />
    とても死に近い美しさ<br />
    血の中の祈り<br />
    血の中の叫び<br />

    血管が狂気はじけ<br />
    私のすべてが崩れる<br />
    私のすべてが狂う時<br />

    悲しい色彩の私の血よ<br />
    お前は私を裏切るだろうか<br />
    それとも<br />
    歪んだ夜に重なる<br />
    黒い摂理の滴り<br />

    戦慄の口づけの後<br />
    私はすこしだけ微笑む<br />
    私はすこしだけ<br />
    微笑む<br />


#### Kana {#kana}

<!--list-separator-->

-  こどく を はかる [Unofficial by j-talk.com from Unofficial Japanese Main by (Evelyn Huang AND Hitomi Cecily) from Heavenly Persona]

    わたくし の きずぐち に ながれこむ<br />
    とても しに ちかい うつくし-さ<br />
    ち の なか の いのり<br />
    ち の なか の さけび<br />

    けっかん が きょうき はじけ<br />
    わたくし の すべて が くずれる<br />
    わたくし の すべて が くるう とき<br />

    かなしい しきさい の わたくし の ち よ<br />
    おまえ は わたくし を うらぎるだろう か<br />
    それ と も<br />
    ゆがんだ よる に かさなる<br />
    くろい せつり の したたり<br />

    こどく を はかる<br />
    せんりつ の くちづけ の あと<br />
    わたくし は すこし だけ ほほえむ<br />
    わたくし は すこし だけ<br />
    ほほえむ<br />


#### Romaji {#romaji}

<!--list-separator-->

-  Kodoku o Hakaru [Unofficial by j-talk.com and Shizuka Star from Unofficial Japanese Main by (Evelyn Huang AND Hitomi Cecily) from Heavenly Persona]

    watashi no kizuguchi ni nagarekomu<br />
    totemo shini chikai utsukushi-sa<br />
    chi no naka no inori<br />
    chi no naka no sakebi<br />

    kekkan ga kyōki hajike<br />
    watashi no subete ga kuzureru toki # added toki<br />
    watashi no subete ga kurū toki<br />

    kanashī shikisai no watashi no chi yo<br />
    omae wa watashi o uragirudarō ka<br />
    sore to mo<br />
    yuganda yoru ni kasanaru<br />
    kuroi setsuri no shitatari<br />

    senritsu no kuchizuke no ato<br />
    watashi wa sukoshi dake hohoemu<br />
    watashi wa sukoshi dake hohoemu<br />

<!--list-separator-->

-  Kodoku o Hakaru [Unofficial by (j-talk.com and Shizuka) Star from Unofficial Japanese Main by (Evelyn Huang AND Hitomi Cecily) from Heavenly Persona] [Live Shizuka]

    watashi no kizuguchi ni nagarekomu<br />
    totemo shini chikai utsukushi-sa<br />
    chi no naka no inori<br />
    chi no naka no sakebi<br />

    kekkan ga kyōki hajike<br />
    watashi no subete ga kuzureru toki # added toki<br />
    watashi no subete ga kurū toki<br />

    yuganda yoru ni kasanaru<br />
    kuroi setsuri no shitatari<br />

    senritsu no kuchizuke no ato<br />
    watashi wa sukoshi dake hohoemu<br />
    watashi wa sukoshi dake hohoemu<br />


### English {#english}


#### Planning for Loneliness [Official from Live Shizuka] {#planning-for-loneliness-official-from-live-shizuka}

The beauty of death that is so close,<br />
flowing into my wounds.<br />
A prayer in my blood.<br />
A cry in my blood.<br />
With a wild feeling<br />
my blood vessels burst open.<br />
A time when I can completely collapse.<br />
A time when I can go completely crazy.<br />
The sad colours of my blood!<br />
Could it be that you will betray me?<br />
Well then...<br />
In the warped night,<br />
the dripping of a heavy black destiny.<br />
After my shivering.<br />
I just give a little giggle.<br />


#### Planning for Loneliness [Unofficial by Evelyn Huang] {#planning-for-loneliness-unofficial-by-evelyn-huang}

It flows into my wounds<br />
A kind of beauty so close to death<br />
Prayers in blood<br />
Screams in blood<br />

My arteries snap open in insanity<br />
When my everything is crumbling<br />
When my everything is going mad<br />

My blood in sad colors<br />
Are you going to betray me?<br />
Or is it<br />
A drip of layered black providence<br />
In a warped night<br />

After a frightening kiss,<br />
I just smile a little bit<br />
I just smile<br />
a little bit<br />


### Portuguese {#portuguese}


#### Planejando-me à solidão [Unofficial by Shizuka Star from Official English on Live Shizuka] {#planejando-me-à-solidão-unofficial-by-shizuka-star-from-official-english-on-live-shizuka}

A beldade da morte que está tão próxima,<br />
fluindo em minhas feridas.<br />
Uma prece em meu sangue.<br />
Um choro em meu sangue.<br />

Com uma ferocidade,<br />
meus vasos sanguíneos estouram.<br />
O dia em que eu colapsarei completamente.<br />
O dia em que eu enlouquecerei plenamente.<br />

Estas tristes cores do meu sangue!<br />
Será possível que você vai me trair?<br />
Pois bem...<br />
Na noite deturpada,<br />
o gotejar de uma sina tão sombria.<br />

Após meu estremecer.<br />
Eu só vou dar uma risadinha.<br />


#### Planejando-me à Solidão [Unofficial by Shizuka Star from Unofficial English by Evelyn Huang] {#planejando-me-à-solidão-unofficial-by-shizuka-star-from-unofficial-english-by-evelyn-huang}

Flui nas minhas feridas,<br />
Um tipo de beldade tão perto da morte<br />
Preces em sangue<br />
Gritos em sangue<br />

Minhas arterias estouram em insanidade<br />
Quando meu tudo desmonorar<br />
Quando meu tudo enlouquecer<br />

Meu sange em tristes cores<br />
Você vai me trair?<br />
Ou será que<br />
Uma gota de um destino sombrio<br />
Em uma noite deturpada<br />

Após um beijo assustador<br />
Eu só sorrio um pouquinho<br />
Eu só sorrio<br />
Um pouquinho<br />


## The Burial of a Shooting Star {#the-burial-of-a-shooting-star}


### English {#english}


#### The Burial of a Shooting Star [Official from Live Shizuka] {#the-burial-of-a-shooting-star-official-from-live-shizuka}

I bite at your white fingers.<br />
The pain of memory.<br />
The pain of desertion.<br />
I've begun a cold quiet daydream,<br />
about a mouthful of apple I can't reach—<br />
where you just lose your bearings<br />
and flee from me,<br />
laughing.<br />
Clasping your white hand,<br />
plating a kiss on your pale red lips,<br />
in the bewitching night,<br />
two people become one shooting star.<br />


## The Night When the Gates Swing Open (扉の開かれる夜, Tobira no Hirakareru Yoru) {#the-night-when-the-gates-swing-open--扉の開かれる夜-tobira-no-hirakareru-yoru}


### Japanese {#japanese}


#### Main {#main}

<!--list-separator-->

-  扉の開かれる夜 [Official from Traditional Aesthetics]

    今宵は扉の開けられる夜<br />
    いにしえの精霊達が鐘を鳴らして<br />
    あなたのまどろむ眠りを覚ますだろう<br />
    今宵は扉が開けられるのだから<br />
    妖精の戯れに空からこぼれ落ちた<br />
    星屑を手のひらに拾い集めよう<br />
    今宵は姿無き者達さえ<br />
    虹をまとって舞い降りるのだから<br />
    今宵は扉の開けられる夜<br />
    離れた者達が一つになる夜<br />


#### Kana {#kana}

<!--list-separator-->

-  とびら の ひらかれる よる

    こよい は とびら の あけられる よる<br />
    いにしえ の せいれい-たち が かね を ならして<br />
    あなた の まどろむ ねむり を さますだろう<br />
    こよい は とびら が あけられる の だ から<br />
    ようせい の たわむれ に そら から こぼれおちた<br />
    ほしくず を てのひら に ひろいあつめよう<br />
    こよい は すがた なき もの-たち さえ<br />
    にじ を まとって まいおりる の だ から<br />
    こよい は とびら の あけられる よる<br />
    はなれた もの-たち が ひと-つ に なる よる<br />


#### Romaji {#romaji}

<!--list-separator-->

-  Tobira no Hirakareru Yoru

    koyoi wa tobira no akerareru yoru<br />
    inishie no seirei-tachi ga kane o narashite<br />
    anata no madoromu nemuri o samasudarō<br />
    koyoi wa tobira ga akerareru no da kara<br />
    yōsei no tawamure ni sora kara koboreochita<br />
    hoshikuzu o tenohira ni hiroiatsumeyō<br />
    koyoi wa sugata naki mono-tachi sae<br />
    niji o matotte maioriru no da kara<br />
    koyoi wa tobira no akerareru yoru<br />
    hanareta mono-tachi ga hito-tsu ni naru yoru<br />


### English {#english}


#### The Night When the Gates Swing Open [Official from Traditional Aesthetics] {#the-night-when-the-gates-swing-open-official-from-traditional-aesthetics}

Tonight's the night when the gates swing open<br />
Spirits from the past are ringing their bells<br />
Awaking you from your slumber<br />
Because tonight the gates swing open<br />
Let's gather in cupped palms those stars<br />
Shaken loose from the sky by dancing sprites<br />
Tonight even those without form<br />
Will flutter to earth on rainbow slides<br />
Tonight's the night when the gates swing open<br />
The night to become one with those who have left us


### Portuguese {#portuguese}


#### A noite em que os portões se abrem [Unofficial by Shizuka Star from Official English from Traditional Aesthetics] {#a-noite-em-que-os-portões-se-abrem-unofficial-by-shizuka-star-from-official-english-from-traditional-aesthetics}

Esta é a noite em que os portões se abrem<br />
Os espíritos do passado badalam seus sinos<br />
acordando-te da tua soneca<br />
Pois hoje à noite os portões se abrem<br />
Com a palma das nossas mãos, vamos juntar aquelas estrelas<br />
jogadas do céu pelos espíritos dançantes<br />
Hoje à noite até aqueles sem forma<br />
descenderão à Terra em arco-íris<br />
Esta é a noite em que os portões se abrem<br />
A noite para nos tornarmos unos com aqueles que nos deixaram<br />
