+++
title = "Musical Style"
author = ["Shizuka Star"]
date = 2020-10-03
lastmod = 2021-02-28T20:13:51-03:00
tags = ["musical-style"]
categories = ["style"]
draft = false
+++

Shizuka's musical style features simple arrangements ([1](#org55c2296), [2](#org6f49fd0)), going from serene and melancholic traditional folk to the Japanese psychedelic harsh noise—a characteristic music contrast ([3](#orgec6e246)) present in their recordings which commonly is developed trough crescendos. It is led primarily by Shizuka's vocals and Maki's guitar: Shizuka's vocals are chant-styled, slow and plaintive—often connoting sadness ([4](#orgc1f28a1)); Maki's guitar is characterized both by a simple, "gently plucked guitar" and by emotive ([5](#orgb2e67d2)) psychedelic-noise solos, as influences from acts like Fushitsuha and Les Rallizes Dénudés ([1](#org55c2296), [3](#orgec6e246)).

Shizuka's music style has notably been described as distinctive ([6](#orgb7d6366), [7](#org3ee1214)), from both the overall "spacey ambiance" and Shizuka's haunting, madrigal-resembling vocals, suggesting a "gothic atmosphere" ([1](#org55c2296)). Mason Jones noted that Shizuka's vocals can be perceived as "not quite perfect", and Bill Meyer described it as "tunelessness"; however, Jones also wrote that "its imperfections perhaps add an emotional edge which an overly-polished voice would eliminate" ([3](#orgec6e246)), and Meyer also correlates writing that Shizuka's vocals are "possessed of the same monomaniacal insistence on her own emotional dislocation as Nico" ([2](#org6f49fd0)).


## References {#references}

<a id="org55c2296"></a>1.  MCFARLANE, Dean. _Shizuka - Shizuka_ [online]. AllMusic. Available from: <https://www.allmusic.com/album/shizuka-mw0001126887>

<a id="org6f49fd0"></a>2.  MEYER, Bill. _Shizuka - Traditional Aesthetics_ [online]. 2008. The Wire. 294. Available from: <https://www.thewire.co.uk/issues/294>

<a id="orgec6e246"></a>3.  JONES, Mason. _Shizuka: “Heavenly Persona”_ [online]. 1995. Ongaku Otaku. 1. Available from: <https://archive.org/details/ongakuotaku1>

<a id="orgc1f28a1"></a>4.  RECORDS, Aquarius and RECORDS, Eclipse. _Shizuka - Tokyo Underground ’95_ [online]. 2001. Last Visible Dog Records. Available from: <http://lvd.4mg.com/030.htm>

<a id="orgb2e67d2"></a>5.  LAIRD, Gavin. _Le Weekend, Stirling, 25th–28th April 2002: Day One_ [online]. 2002. Telstar Ponies. Available from: <https://web.archive.org/web/20030212115630/http://www.telstarponies.com/lwone.htm>

<a id="orgb7d6366"></a>6.  CUMMINGS, Alan. _Shizuka Miura_ [online]. 2010. The Wire. Available from: <https://www.thewire.co.uk/news/18497/shizuka-miura>

<a id="org3ee1214"></a>7.  RECORDS, PSF. _Shizuka / Owari no Nai Yume (DVD)_ [online]. 2010. PSF Records. Available from: <https://web.archive.org/web/20160407120135/http://psfrecords.com/listLPsingle.html>
