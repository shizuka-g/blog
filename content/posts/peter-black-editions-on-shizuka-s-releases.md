+++
title = "Peter (Black Editions) on Shizuka's Releases"
author = ["Shizuka Star"]
date = 2019-11-11T08:13:00-03:00
lastmod = 2021-02-28T20:13:47-03:00
tags = ["black-editions", "peter-kolovos", "heavenly-persona", "no-4", "official", "interview"]
categories = ["release"]
draft = true
+++

Monday last week I got a reply from Peter (Black Editions) regarding Shizuka's releases. Thought posting it here.

> We will be focusing on Shizuka’s releases this upcoming year - and some form of release of No. 4 would be a part of this/ we obviously haven’t gotten into the body of work yet- but please know we will and in a way that I think all fans and also new listeners will appreciate. We’re about increasing the availability of the work itself and the historical information and documentation around it. Thank you for your patience 🙏🏼 All my best, Peter.
