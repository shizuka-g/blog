+++
title = "Press"
author = ["Shizuka Star"]
date = 2021-02-28T18:24:00-03:00
lastmod = 2021-02-28T20:13:46-03:00
tags = ["press"]
categories = ["archive", "press"]
draft = false
+++

## Interviews {#interviews}


### Evelyn "ethereal" Huang. 2001. Rocker (Taiwan). {#evelyn-ethereal-huang-dot-2001-dot-rocker--taiwan--dot}


#### Traditional Chinese (zh-TW) {#traditional-chinese--zh-tw}

靜香: 天界のペルソナ (1994)

昏昏然之迷離噪音 輕飄飄的死亡哥德

曲目リスト
(1)＋
(2)パンドラの匣
(3)少女の口びるに蝶よとまれ
(4)孤独を図る
(5)血まみれの華
(6)水晶の翼
(7)６グラムの星

靜香ＳＨＩＺＵＫＡ
靜香 [三浦靜香] (vocals,guitars)
三浦マキ (guitars)…不失者、裸のラリーズ
早川岳晴 (bass)
Seven (bass)
長原一郎 (piano)

レビュー
アシッド・サイケデリック女性ヴォーカリスト「静香」のファースト・アルバム。ゆらめくようなサウンドと妖しさの中に清楚さを感じる不思議な歌声が絡む90年代サイケデリックの佳作。
小杉淳 (drums)…不失者

最接近天國的異界之音──靜香
採訪／文字：ethereal
翻譯協力：seat

滄涼的低沉女聲在緩緩的民謠撥絃中響起，唱出一段優美而哀傷的旋律，穿插其間的是纖細而具透明感的音符，一切以夢的步調進行著……。你以為就是這樣了，然而慓悍的吉他噪音冷不防地升起，織成一道色彩變幻不定的音牆，彷彿先前深深收斂的情緒全都狂洩在此刻的歇斯底里中，然後一切歸於平靜。這是最接近天國的異界之音，靜香。

不要被名字誤導了，靜香不只是一個人，更是一個真正的樂團，目前的成員包括三浦靜香（主唱、吉他）、三浦真樹（吉他）、小杉淳（鼓）以及SevenZ（貝斯），是日本地下迷幻樂界中的一朵奇杷，曲風游走於陰沉詭異到天籟美聲之間，但靜香與生俱來獨特嗓音與真樹繁複的吉他彈奏是始終不變的印記。

靜香的本業為人形創作，師從日本著名的人形師天野可淡，作品就如同其音樂般陰森細緻，具有遺世獨立的妖異美。約十年前她在丈夫三浦真樹（一個擁有豐富樂團經歷的老練樂手）的協助下開始創作音樂，以圈外人之姿做出了前所未見的感覺，後來甚至取代人形而成為主要的創作領域。華美與破敗，少女與蒼老，安祥與瘋狂，這些看似互相矛盾的素質在靜香的世界中都理所當然地共存，並表現在其詞曲中。第一張錄音室專輯《天界のペルソナ》發行於一九九四年，是張富於空靈歌德感的傑作。

在九五年的《Live Shizuka》中，吉他噪音的運用得到進一步的開發，整體的迷幻味大增，更展現出其現場表演的實力。以唱片發行數而言，靜香的創作量稱不上豐富，但從未間斷的現場演出代表他們對自身藝術的堅持，稀少的錄音作品因此顯得彌足珍貴。而現在，舊團員的回歸已使靜香進入下一個階段，新專輯令人十分期待。

為了深入瞭解靜香的世界，筆者特地前往日本觀賞其表演並與團員進行面對面的訪問，以下即為訪問內容的整理紀錄。

＊以下，靜香指三浦靜香，「靜香」指整個樂團。

──「靜香」是何時、以及怎麼開始的？

靜香：開始創作音樂大約是在一九九二年。我向來喜歡寫詩，所以累積了許多歌詞，不過當時我還不知道如何寫歌。

真樹：然後有次我為她的一首詩譜曲，她見到我寫歌的過程，似乎從中學會了寫歌的方法，漸漸能夠獨立創作音樂。事實上，幾天後我就發現她已唱出了三首歌。

靜香：那三首歌就是我首次演出的曲目。

真樹：最初的三場演出，包括租場地、演唱、彈奏等等，都是由靜香一人獨自完成的。第三次我受邀看她的演出，結果非常好，令我驚訝的是她擁有一種源於自身的原創性。於是靜香與我便以雙人組的型態展開活動。

──最初寫下的三首歌是什麼？

靜香：〈流れ星の埋葬〉、〈パンドラの匣〉、和〈あやまち〉。

──我個人最喜歡的歌是〈孤獨を圖る〉。

靜香：謝謝，那首歌對我來說也十分重要。十四年前我正與胃癌搏鬥，吐了很多血，情況嚴重到連醫生都差點宣告放棄了。當時我有個才一歲半的孩子，為了他，我一直在內心吶喊著「不能死！」，而最後我奇蹟似地活下來了。這段體驗促使我寫下「孤獨を圖る」。因為這首歌對我們的意義是如此深遠，我們總是在表演曲目中排入這首歌。

──（對靜香）印象所及，妳在日本主要是以人形師的身分為人所知。請問妳是如何看待自己的？人形創作與妳的音樂之間有任何關係嗎？

靜香：是的，早在開始創作音樂之前我已是個人形師，引領我進入人形的世界的是已逝的天野可淡。我們的首張專輯《天界のペルソナ》的重要性之一，便在於它是獻給可淡的。至於音樂與人形的關係……當靈感來時，我就跟隨靈感尋找創作的塑材，最後可能以音樂或人形的型態呈現。這兩者皆是我的私宇宙的產物，它們來自同樣的靈感，而所有的靈感皆來自於我的經驗。不過，這幾年間我將全部的心力投注在音樂上，已經沒有餘力創作人形了。

──「靜香」似乎在一九九五年後有活動趨緩的傾向，而現在正與舊團員重新開始。為何會有這麼長的空檔？

真樹：我們從沒解散過，直到現在始終持續進行著。在一開始時，小杉是鼓手，然後第一個貝斯手也加入了，不過他跟不上我們的步伐，bass line得由我來寫。這是個大問題，因為我們習慣在錄音室裡現場完成編曲。在第一張錄音室專輯發行後，我們認識了SevenZ，他原本是個吉他手，不過當起貝斯手來也十分稱職，而且能夠寫出很漂亮的bass line，我們便邀請他加入樂團，於是原初的「靜香」團隊在一九九五年正式結成。但是不久後SevenZ和小杉因為私人理由先後脫退，之後我們又換了幾次成員，但始終覺得原本的成員是最好的，所以一直在等他們回來。

──談談美國tour的經驗吧。

靜香：Tour是由Mason Jones（註一）籌畫的，Seven和小杉沒有參加。我們去了洛杉磯和舊金山，舊金山那邊有許多歌迷，觀眾的整體反應也相當好。此外我們還去了當地的廣播電台兩次，其中一次在電台做了現場表演。相較於日本的觀眾，美國的觀眾在反應上比較主動而熱烈。

真樹：所有的觀眾都隨著音樂起舞，這讓我很開心（笑）。

靜香：今年十月或十一月，我們將應The Wire雜誌之邀到蘇格蘭參加一個他們舉辦的音樂節。

──你們最喜歡的樂團／藝術家呢？

靜香：若說到日本迷幻音樂，那就是Les Rallizes DeNudes（註二）了。

真樹：我喜歡「靜香」、天野可淡、山口富士夫（註三）、和水谷孝（Rallizes的團長）。Les Rallizes DeNudes已經好幾年沒出現過了，不過我曾是這個樂團的成員。

──（對真樹）除了「靜香」以外，你還加入過哪些樂團？

真樹：最有名的是Les Rallizes DeNudes和不失者（註四），另外我和Friction（註五）的前吉他手Lapis一起做過音樂，還有很多其它的……比如說DJ Krush，我也和他合作過。

──（對真樹）你的吉他獨奏似乎傾向於偏離主旋律，有何特殊理由嗎？

真樹：嗯……為什麼會這麼覺得呢？那對我來說是正常的。置身於音樂中，我總是自然而然地彈奏，試著找到最適合的句法，沒有刻意偏離主旋律的意思。不過，我想這問題有一部份是對的。因為「靜香」的歌大部分都有緩慢而優雅的主結構，我的吉他噪音獨奏也許有使那部分突顯出來的效果，而強化整首歌的張力。這就是就是所謂的光與影、天使與惡魔吧（笑）。

──不過〈世に殘す歌〉似乎有些不同，我覺得這首歌中吉他與主旋律配合得較緊。

真樹：我是覺得沒什麼差別。不過這首歌有旋律在先，對我們來說並不平常，也許這就是為什麼它聽起來會不一樣的原因吧。

靜香：這首歌的產生比較特殊一點。主旋律在我腦中縈繞了有半年之久，而我一直想不出能夠與之匹配的歌詞。後來我讀到《ほつまつたえ（世に殘す歌）》，一本關於日本神祇的文學作品，發現其中有段文字跟主旋律非常契合，便以這段文字當作歌詞完成這首歌。

真樹：通常我們的創作過程是先聽靜香將詞曲唱過，一起jam看看，再花些時間想想怎麼編曲。不過在做〈世に殘す歌〉時，彷彿每一個音符已事先找到它的位置般，整個編曲在first take中很順利地就完成了。那次的錄音收錄在PSF的合輯《TokyoFlashback 4》中。

──對當今的日本迷幻音樂有何看法？

真樹：這裡有很多所謂的迷幻樂團，不過我對音樂的歸類沒興趣，所以並不清楚……我知道一些相當不錯的樂團，可惜它們並不是全都很有名。

靜香：我認為只有Les Rallizes DeNudes和「靜香」是日本真正的迷幻樂團。

──可否用一句話來描述你們所做的音樂？

真樹：就是搖滾吧……我所謂搖滾指的不只是一種音樂型態，也是一種生活方式。最近我想到一個很好的關鍵句：「世界上最緩慢的搖滾樂」。

靜香：不過這並非有意識的，而是我的自然步調。我以我的自然步調創作音樂，所以寫出來的歌大部分是緩慢的。不過現在我們也開始嘗試做一些快歌。

真樹：靜香對此似乎有些自卑感，不過我覺得緩慢其實有許多可能性，「究極的緩慢」是值得追求的。也許藉此我們可以找到一個新視野，創造出一種新型態的迷幻音樂。

──新專輯的進度如何？

真樹：仍在籌備中。實際上，兩三個月前我們錄了三首歌，結果令人十分滿意，將交由美國的獨立廠牌Detector發行黑膠唱片的單曲。現在，「靜香」已再度開始活動，安排tour行程與錄製新專輯即是其中的一部份。我們對未來可能發生的一切感到十分期待。

註一：美國獨立廠牌Charnel Music的負責人，本身也是一位噪音藝術家，其製作的《Land of the Rising Noise》系列合輯對引介日本地下音樂至西方有相當的貢獻。

註二：日本傳奇性的迷幻樂團，一九六七年結成以來只發表過三張專輯，張張皆為貴重品。

註三：獨立音樂家，曾為「村八分」、Les Rallizes DeNudes與Counter CultureBand的團員。

註四：日本國寶級吉他手／聲音藝術家灰野敬二所領軍的前衛迷幻樂團，見《搖滾客》第四期的專文。

註五：歷史長久的日本高速噪音團，核心人物Reck與Chiko-Hige曾至紐約吸收其NoWave scene的養分。

靜香 Discography：
天界のペルソナ／PSF／1994

Live Shizuka／Persona Non Grata／1995
Tokyo Underground '95（CD-R）／The Last Visible Dog／2000


#### English {#english}

<!--list-separator-->

-  The Closest "the Other Space" Sound to the Kingdom of Heaven—Shizuka [Unofficial by native Chinese translator and Shizuka Star]

    <!--list-separator-->

    -  Shizuka: Heavenly Persona (1994)

        <!--list-separator-->

        -  Tracklist

            1.  Ten
            2.  Pandora's Box
            3.  Butterfly, Alight on a Girl's Lips
            4.  Planning for Loneliness
            5.  Bloodspattered Blossom
            6.  Crystal Wings
            7.  6g Star

        <!--list-separator-->

        -  Members

            -   Shizuka
            -   Shizuka (Shizuka Miura): vocals, guitars
            -   Maki Miura: guitars
            -   idk: bass
            -   Tetsuya "Seven" Mugishima: bass
            -   Ichirou Nagahara: piano

        <!--list-separator-->

        -  Review

            The first album by acid psychedelic female vocalist Shizuka. This is a fine piece of '90s psychedelic music with a shimmering sound and a mysterious voice that feels innocent in the midst of mystery.

            Jun Kosugi (drums)... Fushitsusha.

    <!--list-separator-->

    -  Credits

        -   Interview and text: etheral
        -   Translation support: seat

    <!--list-separator-->

    -  Description

        The cold, low female voice rose from the slow folk plucking, singing a beautiful and sad melody, interspersed with delicate and transparent musical notes, all at the pace of a dream... You think that's it, but then the tough guitar noise suddenly rises, weaving a wall of shifting colors, as if all the previously deeply convergent emotions are released into the hysteria at this moment, and then everything is calm. This is the closest "The Other Space" sound to the kingdom of heaven—Shizuka.

        Don't be misled by the name, Shizuka is not only a person, but also a real orchestra. The current members include Shizuka Miura (lead singer, guitar), Maki Miura (guitar), Jun Kosugi (drum), and SevenZ (bass). The band is an outgrowth of Japan's underground psychedelic music scene, with a style that ranges from the gloomy strange to the Paradise Music，but Shizuka's inborn unique voice and the intricate guitar playing of Maki have been a constant brand.

        Shizuka's own work is creation of dolls. Under the famous Japanese dollmaking teacher Katan Amano, her work is as dark as her music, which has the unique rare beauty of the world. About a decade ago, with the help of her husband Maki Miura (a seasoned musician with rich orchestra experience), she began to create music, making unprecedented feelings as an outsider, and later replacing dollmaking as the main creative field of her own. Beautiful and dilapidated, young girls and old age, peaceful and crazy; these seemingly contradictory qualities naturally coexist in the world of Shizuka and were manifested in her songs. Her first studio album, "Heavenly Persona", was released in 1994 and that is an ethereal masterpiece.

        In "Live Shizuka", released in 1995, the use of guitar noise was further developed, and the overall psychedelic flavor increased greatly, showing the strength of its live performance. In terms of the number of records released, Shizuka's creation is not rich, but the uninterrupted live performance represents their adherence to their own art, so the rare recording works appear precious. And now, the return of the old members has made Shizuka into the next stage of their career, and the new album is very expected. In order to have a deeper understanding of Shizuka's world, the author specially went to Japan to watch their performance and had a face to face interview with the members. The following is a summary of the interview contents.

    <!--list-separator-->

    -  Interview

        Below: Shizuka refers to Shizuka Miura and Shizuka (band) refers to the whole orchestra.

        -   When and how did Shizuka (band) start?
            -   Shizuka: The beginning of my music creation was in about 1992. I always liked to write poetry, so I accumulated a lot of lyrics, but I didn't know how to write songs.
            -   Maki: Then once I wrote music for one of her poems, she saw me write songs, and seemed to learn how to write songs, and she gradually become able to create music independently. In fact, I found out a few days later that she had made three songs.
            -   Shizuka: Those three songs are my first performance.
            -   Maki: The first three performances, including renting, singing, playing and so on, were done by herself alone. The third time I was invited to see her performance, the result was very good. To my surprise, she had a kind of originality from her own. So Shizuka and I began some activities in the form of a double group.

        <!--listend-->

        -   What were the three songs that she originally wrote?
            -   Shizuka: "The Burial of a Shooting Star", "Pandora's Box", and "Ayamachi".

        <!--listend-->

        -   My favorite song is "Planning for Loneliness".
            -   Shizuka: Thank you! That song is also very important to me. Fourteen years ago I was fighting stomach cancer, spitting a lot of blood, so serious that even the doctor almost gave up. At that time I had a child only one and a half years old, for him, I have been shouting in my heart, "I can not die!". And in the end I lived like a miracle. This experience prompted me to write "Planning for Loneliness". Because this song is so far-reaching to us, we always drain it into the performance repertoire.

        <!--listend-->

        -   (to Shizuka) For all I know, you are mainly known in Japan as a dollmaker. What do you think of yourself? Is there any relationship between dollmaking and your music?
            -   Shizuka: Yes, I was a personal stylist long before I started writing music. What led me into the world of dollmaking was the late Katan Amano. One of the importance of our first album "Heavenly Persona" is that it is dedicated to Katan Amano. As for the relationship between music and doll... When inspiration comes, I follow the inspiration to find the material, which may eventually be presented in music or doll. Both of them are the products of my private universe, they come from the same inspiration, and all inspiration comes from my experience. However, in the past few years, I have invested all my heart in music and have no spare power to create dolls.

        <!--listend-->

        -   Shizuka (band) seems to have a tendency to slow down after 1995, but is now starting again with the old members. Why is there such a long gap?
            -   Maki: We have never dissolved, and we continue to do that until now. At the beginning, Kosugi was the drummer, and then the first bass player joined, but he couldn't keep up with us, so the bass line had to be written by myself. This is a big problem, because we are used to doing the choreography in the studio. When the first studio album was released, we met SevenZ. He was a guitarist, but he's also a pretty good bass player and could write beautifully bass line，so we invited him to join the band. So the original Shizuka (band) team was formally formed in 1995. But soon after SevenZ and Kosugi retired for personal reasons. Then we changed members several times, but always felt that the original members were the best, so we had been waiting for them to come back.

        <!--listend-->

        -   Just talk about the American tour experience.
            -   Shizuka: The tour was drawn by Mason Jones (Note 1). Seven and Kosugi did not participate. We went to Los Angeles and San Francisco, where there were a lot of fans and the audience responded very well. In addition, we went to the local radio station twice, one of which we made a live performance on the radio station. Compared with the Japanese audience, the American audience is more active and enthusiastic.
            -   Maki: All the audience will dance to the music, which makes me happy (laugh).
            -   Shizuka: We're going to Scotland at the invitation of The Wire magazine for a music festival they're holding.

        <!--listend-->

        -   Who is your favorite orchestra/artist?
            -   Shizuka: If you talk about Japanese psychedelic music, it's Les Rallizes Denudes (note 2).
            -   Maki: I like Shizuka (band), Katan Amano, Fujio Yamaguchi (Note 3), and Takashi Mizutani (head of Rallizes). Les Rallizes Dénudés haven't been around for years, but I was a member of the band.

        <!--listend-->

        -   (to Maki) Besides Shizuka (band), which bands have you joined?
            -   Maki: The most famous are Les Rallizes Dénudés and Fushitsusha (note 4). I also did music with Lapis, the former guitarist of Friction. There are many other artists... For example, DJ Krush, I worked with him.

        <!--listend-->

        -   (to Maki) Your guitar solo seems to tend to deviate from the main melody. Is there any special reason?
            -   Maki: Um... Why do you think so? That's normal for me. Surround myself with music, I always play naturally, trying to find the most suitable syntax, without deliberately deviating from the main melody. But I think part of this problem is right. Because most of Shizuka's (band) songs have a slow and elegant main structure, my guitar noisy solo may have the effect of highlighting that part to strengthen the tension of the whole song. This is the so-called light and shadow, angel and devil (laugh).

        <!--listend-->

        -   But the song "A Song for the World Left Behind" seems a little different, I think the guitar and the main melody in this song are closely coordinated.
            -   Maki: I think it's no difference between that. But the premise of this song is the melody, it is not ordinary to us, perhaps this is why it sounds different.
            -   Shizuka: The production of this song is a bit more special. The main melody lingered in my mind for half a year, because I could not think of the lyrics that could match it. Later I read "Hotsuma Tsutae" ("A Song for the World Left Behind"), a literary work about Japanese gods, and found that there is a paragraph of the text and the main melody is very suitable, so I finished the song with these words as the lyrics.
            -   Maki: Usually our creative process is to listen to Shizuka sing the lyrics once, think about them together, and then spend some time thinking about how to compose the songs. While composing "A Song for the World Left Behind", as if each note had found its place in advance, the whole choreography was completed smoothly in the first take. The recording was included in the PSF's album "Tokyo Flashback 4".

        <!--listend-->

        -   Could you please describe your music in one sentence?
            -   Maki: It's just "rock and roll"... What I called "rock" is not just a musical style, but also a lifestyle. Recently I thought of a good key sentence to describe that: "the slowest rock music in the world".
            -   Shizuka: Though this is not conscious, but my natural pace. I write music at my natural pace, so most of the songs are slow. But now we are also trying to do some quick songs.
            -   Maki: Shizuka seems to have a sense of inferiority, but I think there are many possibilities for slowness. Extreme slowness is worth pursuing. Perhaps we can find a new vision and create a new type of psychedelic music.


## Reviews {#reviews}


### Heavenly Persona {#heavenly-persona}


#### Mason Jones. Shizuka: "Heeavenly Persona". Ongaku Otaku 1. {#mason-jones-dot-shizuka-heeavenly-persona-dot-ongaku-otaku-1-dot}

CD, P.S.F. Records, PSFD-52

File this beautiful CD somewhere between the hazy dreaminess of the best 4AD releases and the meaty psychedelic fuzziness of Fushitsusha and other Japanese "psychedelic" bands. Mix in a bit of madrigal and traditional folksongs in a wash of reverb, and you're getting the idea of what's up here. The opening track is a deceptively noisy feedback-psych jam, which then calms into the second track's gentle plucked guitar below Shizuka's chanted vocals. Others include some very flighty almost pop-sweet numbers, but the guitar has a way of suddenly sweeping up into some amazing fuzzed-out acrobatics, courtesy of Miura Maki (also known for his contributions to Fushitsusha). The strings on the sixth track are particularly effective, and add a beautiful, yet uneasy, atmosphere to Shizuka's plaintive vocals. Some people might find her voice to be not quite perfect, which is true. Yet its imperfections perhaps add an emotional edge which an overly- polished voice would eliminate. I particularly like the moments where the harsh, chaotic guitar intermixes with the melodic elements. The method is similar to that used on the Slap Happy Humphrey album, but the practice is quite different. This is a very fine album, and I'll look forward to more.


#### Dean McFarlane. Shizuka - Shizuka. All Music. {#dean-mcfarlane-dot-shizuka-shizuka-dot-all-music-dot}

This Japanese underground group recorded their debut for the PSF label after making a standout appearance on the label's Tokyo Flashback 3 compilation. Their dark, ethereal sound makes them the perfect bridge between the psychedelic overload of fellow Tokyo underground acts Fushitsusha and the melancholy Baroque-folk of Ghost. Shizuka's self-titled debut is strikingly accessible, considering that it appears on one of the most courageous of experimental labels. PSF typically deals in free improvisation, noise, and psychedelia, and while Shizuka certainly address the influence of said genres, they embody this influence in an entirely unique way. While this trio is certainly falls under the umbrella of the latter -- the spacey ambience and haunting female vocals suggest a gothic atmosphere laced with folk instrumentation. Piano and cello embellish the simplistic arrangements of guitar and voice delivered in lilting patterns. At the apex of every song a corrosive Keiji Haino-esque guitar often cuts through the tranquil proceedings, drenching the calm in feedback and noise. These tantrums of chaos sound just as awkwardly juxtaposed, yet strangely they make perfect sense in expressing the catharsis that the album is always hinting at.


### Live Shizuka {#live-shizuka}


#### Andrea Moed. Shizuka - Live Shizuka - Persona Non Grata. CMJ. {#andrea-moed-dot-shizuka-live-shizuka-persona-non-grata-dot-cmj-dot}

It's a good world when a goth queen like Shizuka (heavy eye makeup, album art featuring sickly-looking dolls, sample lyric: "The flowers that I've been growing in my body for the end look beautiful") can get members of the artful noise band Fushitsusha to play on her record. _Live Shizuka_ (so called although there is or nothing to indicate that is a live album) is an engaging mix of gently-paced trance rock with rumbling feedback arias. Shizuka is a highly distinctive vocalist, with an open, searching quality that makes one think of monks (both Kyoto and Benedictine). But what most separates this combo from other recent neo-psychedelia is their thoughtfully restrained use of guitar pedals. They work less with sound that's obviously twisted than they do with subtle evocations of ringing phones or crickets at night. Even the most 4AD-friendly track, "Heavenly Persona," is clearly distinguished by players used to more deviant time signatures and improv. The freakouts of guitarist Miura Maki are tempestuous enough to please any noise fan. In particular, his work on "The Burial Of A Shooting Star" sounds perfect, if unfamiliar stuck with a song context.---Andrea Moed

**DATALOG: Released Sep. 25.**

**FILE UNDER: High-contrast psychedelia.**

**R.I.Y.L: Fushitsusha, Cranes, mellower Hendrix.**


#### Dean Suzuki. 1996. Shizuka - Live Shizuka. Sonic Options Network. {#dean-suzuki-dot-1996-dot-shizuka-live-shizuka-dot-sonic-options-network-dot}

SHIZUKA • Live Shizuka

This is the second album by the Japanese group featuring guitarist Miura Maki, formerly with Haino Keiji's Fushitsusha, drummer Kosugui Jun, currently with Fushitsusha, and the mysterious chanteuse who provides the band with its name. Shizuka brings together the gossamer dreamscapes of earlier 4AD acts and the seemingly incompatible, seething, fuzz-drenched neopsychedelia which is currently the rage in Japan. To top it off, Shizuka's singing is frequently off the mark, even unabashedly off key. Rather than trying to get around what might be perceived as a short-coming, the band sees it as part of the charm and mystery of their work. The odd singing and sensuousness and intensity of the music make comparisons with some of rock's quirkier stylists—Skip Spence or Syd Barrett for instance—inevitable if not accurate. (Persona Non Grata, Box 146, Village Sta., NYC 10014)---Dean Suzuki


#### Shizuka - Live Shizuka. The Wire. {#shizuka-live-shizuka-dot-the-wire-dot}

In which psychedelia and folk cross paths to occasionally wry effect. Singer/guitarist and femme fatale Shizuka has the plaintive persuasion of Robert Wyatt, the tonal waywardness of Nico, a nice line in dark, melancholic atmospheres, and the formidable services of Miura Maki, formerly of Keiji Haino's Fushitsusha, who adds some searing guitar figures in the in the Hainoesque manner.


### Traditional Aesthetics {#traditional-aesthetics}


#### Bill Meyer. Shizuka Traditional Aesthetics PSF CD. The Wire #294. {#bill-meyer-dot-shizuka-traditional-aesthetics-psf-cd-dot-the-wire-294-dot}

Shizuka Miura's group were a peripheral presence in the Japanese psych underground, but the discovery of these live tapes from a 1995 show is a matter of some excitement among the cognoscenti, and for very good reason. The group play like they're trying to burrow down a temporal wormhole into another dimension rather than simply induce a slightly altered state in a few black-clad onlookers in cool sunglasses. Shizuka's songs are certainly simplistic, and she's hardly the most gifted of performers, but in alliance with her musicians – including guitarist Maki Miura and drummer Jun Kosugi (both of Fushitsusha fame) – something of real and rare intensity is conjured.

She delivers her fragile elegies in a haunted bellow, flirting with tunelessness but possessed of the same monomaniacal insistence on her own emotional dislocation as Nico. She sings "A blinding light suddenly shines out/Brighter than any star" in exactly the same tone as "I will surely go to hell/Friendless and alone". Over its 15 minute duration, "Heavenly Persona" keeps drifting loose and then mustering itself for another slow-motion assault, segueing gradually into "A Song For The World Left Behind", which, impressively, sounds as devastatingly forlorn as its title.

Maki Miura, meanwhile, runs through the well-worn catalogue of psych guitar pyrotechnics, yet somehow his echo-drenched waves of distortion sound invigoratingly fresh. Miura's solos spill out of him with a violent urgency that would surely do his mentor Haino-san proud. His playing here goes so far beyond self-indulgence that it seems to come back to meet itself on the other side; it's completely obvious that he has no other choice but to open the floodgates and just bleed ([1](#org24bfa67)).


## References {#references}

<a id="org24bfa67"></a>1.  MEYER, Bill. _Shizuka - Traditional Aesthetics_ [online]. 2008. The Wire. 294. Available from: <https://www.thewire.co.uk/issues/294>
