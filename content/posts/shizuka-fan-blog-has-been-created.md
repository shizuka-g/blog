+++
title = "Shizuka Fan Blog Has Been Created"
author = ["Shizuka Star"]
date = 2020-09-05T04:53:00-03:00
lastmod = 2021-02-28T20:13:45-03:00
tags = ["blog", "reddit", "license", "cc0"]
categories = ["announcement"]
draft = false
+++

Just created this blog dedicated to the Japanese rock band Shizuka, and to its foundress Shizuka Miura. All my original text will be licensed under the CC0 license. Most media (images, videos, etc) for illustration purposes, and quoted text, I probably don't have copyright permissions, unless otherwise stated.


## Past and Future Posts {#past-and-future-posts}

Past posts include some things previously published by me on [r/Shizuka](https://reddit.com/r/shizuka). Future (from now on) posts will be new stuff. Content suggestions are welcome.
