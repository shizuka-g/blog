+++
title = "Song's Meanings/Themes"
author = ["Shizuka Star"]
date = 2020-06-14T03:02:00-03:00
lastmod = 2021-02-28T20:13:50-03:00
tags = ["song", "notes", "lyrics", "emotion", "doll", "shizuka-miura", "katan-amano"]
categories = ["symbolism"]
draft = false
+++

I don't know Japanese, so I can't accurately analyze the songs' meanings. What I will say is based on what I found about Shizuka, on English translations of their lyrics, and my opinion/insights.

Shizuka music themes are mostly, if not exclusively, Shizuka Miura's personality/likings, life experiences, points of view, and career as a dollmaker. Some points:

-   Shizuka is sadness (if not depressive).
-   Shizuka has this lament/dedication for the dead (elegy). It is dirge.
    -   Heavenly Persona, by Shizuka Miura words, was a dedication to Kantan Amano, who had died in 1991, 1 year before Shizuka started her musical career. (I think Katan's death had some psychological impact on Shizuka).
    -   The lyrics of "The Night When the Gates Swing Open" is very elegy-like.
-   Shizuka Miura was inspired by literature.
    -   Shizuka was inspired by the Japanese legendary epic poem Hotsuma Tsutae. She essentially got verbatim text from it to make the lyrics for at least two songs: "Awanorata", and "A Song for the World Left Behind".
    -   I suspect the first song of Owari no Nai Yume, "魚服記" ("Gyofukuki"), is a reference to Osamu Dazai's story of the same name. It is a fantasy story about a girl who turns into a fish and involves suicide.
    -   I also suspect Shizuka was influenced by Hildegard of Bingen's works, by her literature and/or music.
    -   Shizuka Miura seems to have been a model for a character in Maki Kusumoto's manga Kiss xxxx. Might or might not have inspired Shizuka for something.
-   Shizuka was inspired by her past struggles.
    -   By Shizuka Miura's words, the song "Planning for Loneliness" was written by her based on the experience of having stomach cancer and a child of one and a half years old at the same time. It makes sense if you read the lyrics English translation.
-   There was a relationship between the music and Shizuka's dolls.
    -   If you see Shizuka albums artwork, they present pictures of Shizuka's dolls. Since her dolls have a Gothic appeal, it fits nicely with the band's style.
    -   Some of the shows were also an exposition for Shizuka's dolls.
    -   Shizuka Miura, when asked if there was a relationship between her music and her doll work, said that when she got inspired, she would put the effort to create an artistic product, either music or doll.
-   Shizuka themes nature while talking about her stories.
    -   You will read her saying about the sun, stars, the sky, the night, the wind, flowers, lands.
    -   An interesting quote from her found in Telstar Ponies' LP Voices From the New Music: "How can one enjoy the smell of nature, knowing full well she is on the conspiracy of time? At what cost fate?"
-   She talks about negative stuff, sometimes romanticizing it
    -   Negative emotions like loneliness, sorrow...
    -   Negative places like hell, cursed lands...
    -   She talks about injuries, pain, and death...
    -   She talks about heaven and why she wants to get there, although she
        believes she will go to hell instead.
-   She will mention human-body elements: hands, lips, blood, blood vessels...
-   She seems to talk about romantic relationships, but I'm not sure.
