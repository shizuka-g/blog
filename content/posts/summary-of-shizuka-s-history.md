+++
title = "Summary of Shizuka's History"
author = ["Shizuka Star"]
date = 2020-06-14T03:02:00-03:00
lastmod = 2021-02-28T20:13:48-03:00
tags = ["shizuka-miura", "maki-miura", "jun-kosugi", "tomoya-hirata", "tetsuya-seven-mugishima", "katan-amano", "hiatus"]
categories = ["history"]
draft = false
+++

In the Tokyo of 1992, there is this cool girl Shizuka who is a doll artist (she was a protegee of master dollmaker Katan Amano), but who also likes writing poetry. Someday she sees a guy named Maki Miura (maybe already in a relationship/married with Shizuka) composing music, and then Shizuka thinks: "What if I get my poetry and create music?". She pretty much does it: begin composing and even doing some gigs, all by herself. Maki gets impressed by her skills, and then Shizuka and Maki make a duo. Some demo cassettes are self-released (Shizuka's rarest releases by today). After, 2 musicians join them, a drummer, Jun Kosugi (ex-Fushitsusha), and a bassist, Tomoya Hirata (Shizuka's most mysterious member). They do some gigs in Japan and release 3 albums, one being _Heavenly Persona_. In 1995, this guy with the artist name Seven joins as the bassist, replacing the previous one. They manage to do 3 gigs, 2 of which would be released as live albums in the 2000s. By this time, Shizuka and Maki manage to make a tour in the US. Then they enter hiatus.

Shizuka regroups in 2000, when they release a live album, _Tokyo Underground '95_, and also manage to perform in a festival hold in Scotland. From here on they probably only do gigs through Japan, mostly Tokyo. In 2008, they release another live album recorded in '95, Traditional Aesthetics. In December of that year, they record their last album, _Owari no Nai Yume_, which would be released as a tribute to Shizuka Miura after her death in early 2010. Shizuka's history ends here. I guess because Shizuka was really Shizuka's band, it has her personality, she led it, wouldn't make sense for Maki to keep up with the band, and because of the way Shizuka died (suicide).
