+++
title = "Upcoming New Shizuka Live Album, Paradise of Delusion, to be Released on Vinyl by An'archives"
author = ["Shizuka Star"]
date = 2020-06-06T23:00:00-03:00
lastmod = 2021-02-28T20:13:48-03:00
tags = ["an-archives", "cedric-lerouley", "interview", "paradise-of-delusion", "vinyl", "link", "highlight", "tracklist", "commerce", "database"]
categories = ["release", "album", "live", "paradise-of-delusion"]
draft = false
+++

<div class="INFO">
  <div></div>

The label has been having issues to release this album as noted in the redacted [Where to Buy](#orgb16b8b2) section.

</div>


## Introduction {#introduction}

Since 2019, a new Shizuka **live album**, titled _**Paradise of Delusion**_, has been announced to be released by French label [An'archives](https://anarchives.com/), which seems to specialize in Japanese artists. This pre-announced album, to be released on the double-sided **vinyl** format, has a total of **8 tracks** (I verified and found that 2 of them are songs never released before, 2 were only released in 2 of Shizuka's rare cassettes, and 1 other only released on a various artists compilation) taken from a previously **unreleased live recording in 2001**, one year after Shizuka's reunition and release of _Tokyo Underground '95_. This is the first time Shizuka will be released on vinyl, and the first album to be released since Owari no Nai Yume, which **marks a decade of the band dissolution** following Shizuka Miura's death.

{{< figure src="/blog/image/55acf73a-71cb-4913-86c5-ba7f5e11002c.png" caption="Figure 1: Front cover of _Paradise of Delusion_." >}}


## Highlights {#highlights}

Yesterday (5 June 2020), I got in contact with the **An'archives head**, **Cédric Lerouley**, who is also responsible for _Paradise of Delusion_, and clarified in detail some pertinent questions for me. My highlights and insights from Cédric's answers are:

-   The **release is endorsed by Maki Miura**, who also provided its contents.
-   The tracks were **live recorded in 2001** by a former Shizuka member (not specified).
-   The members present on this recording are **Shizuka Miura**, **Maki Miura**, **Jun Kosugi**, and **Tetsuya "Seven" Mugishima**.
-   The artwork is from Shizuka and Maki's collection.
-   **N. Takafuji is the photographer** of the picture of Shizuka Miura on the vinyl's front cover.
-   There are **two editions to be released on the vinyl** medium:
    -   The first, or the **regular-edition**, is limited to **275 copies**, which will "come in a silkscreened paper portfolio with a different obi". It is the one available for pre-order in some **Web retailers**, and will be distributed by a company from UK.
    -   The second, or **sub-edition**, is limited to **75 copies**, and will be available for **purchase directly from An'archives**.
-   As I understood, there **won't be any release on the digital format**, platforms or services.
-   **Cédric** is taking care of the **vinyls production**, ~~and it will take _at least_ 1 to 2 months for the vinyls to be ready~~, so let's wait patiently for it.

    {{< figure src="/blog/image/fe9c85ba-7811-4b1f-bdda-aa98cb744426.jpg" caption="Figure 2: Artwork that will come with the vinyl." >}}


## Questions and Answers {#questions-and-answers}

The following are the questions I made and the respective answers from Cédric.

How and where did you get these tracks? Was it ripped from other releases (some tracks have the same title that some Shizuka's cassettes etc have), or is it something new/exclusive?

> This is an unreleased live and exclusive recording from 2001 that was recorded by one [of] the band member[s]. It seems this is the only/last [u]nknown recording that has been tracked by [Maki] Miura until now. Members were then Shizuka, Maki, [Jun] Kosugi, and [Tetsuya] Seven.

I saw on hhv.de the tracks were live recorded in 2001. In an interview from 2001, Maki Miura cites they were working on a new album (but it was never released). Also, some years ago, Tetsuya "Seven" Mugishima tweeted saying there were these Shizuka's recordings from circa 2001 that needed some work before being released. Has "Paradise of Delusion" anything to do with any
of this?

> No idea if they were talking about this one in 2001, but I don't think [so]. I've been told some recordings are definitively lost.

How and where did you get the artwork? The front cover is a published image since at least 2017 on "Tokyo Flashback: PSF", released by Super Fuji Discs.

> Front cover picture was provided by [Maki] Miura, this one was shot by N. Takafuji, other visuals (but I changed and adapted them) come from Shizuka and Maki's collection.

Will you release it through your Bandcamp (both the vinyl and digital version)? I saw a tweet from you saying you'll release a sub-edition. Will this sub-edition be any different from these various pre-orders (like the ones on deejay.de, hhv.de, etc)?

> Yes I'll offer a sub edition ([limited] to 75) that would be only available from the label/me, the regular edition will come in a silkscreened paper portfolio/wrap with a different obi.
>
> This one Will be only an exclusive vinyl release, no digital will be offered.
>
> I met a lot of problems with the jackets as they are tip-on / old style jackets, they are printed with offset and a silkscreened layer needs to be added on the back. All copies will come with a silkscreened obi, a 3 postcard set, a A3 duotone 2 sided insert and 2 other inserts. Everything is ready excepted those jackets who are now in California.
>
> I only have one distributor in UK, I don't directly work with those companies in Germany, so no idea if they will really have them in stock. But usually I don't have a lot of distribution, I know some retailers always list but sometimes don't have physically the releases.

Additional information.

> I hope it's ready in 4-6 weeks, but I can' t really say because I've already rejected the jackets 2 times and now because of COVID-19 shipping is taking some extra delay.


## Tracklist {#tracklist}

Tracklist of the regular edition as listed by some Web retailers. Subitems indicate previous releases that have tracks with the same title (not the same recording though).

-   A1. For You (君のために, Kimi no Tame ni)
    -   Shizuka III, A1 (circa 1993)
-   A2. They've Come for You (迎えに来たよ, Mukae ni Kita Yo)
-   A3. The Flower of Finality (終末の華, Syuumatsu no Hana)
    -   Shizuka II, A2 (circa 1993)
    -   静香, 7 (circa 1995, recorded in 1994)
    -   Hikyoku no Seiseki: Live at Manda-La2 1993 & Studio Ams 1994, 7 (2009)
-   A4. Paradise of Delusion (妄想の楽園, Mousou no Rakuen)
    -   No. 4, B6 (circa 1993)
-   B1. Corolla (花冠, Kakan)
-   B2. Account of a Watery Grave (魚腹記, Gyofuku-ki)
    -   Owari no Nai Yume, 2 (2010, recorded in 2008)
-   B3. Premonition (きざし, Kizashi)
    -   Do the Independence and Bridge Build Burn By Yourself (2009)
-   B4. The Night When the Door Opens (扉の開かれる夜, Tobira no Hirakareru
    Yoru)
    -   Traditional Aesthetics, 2 (2008, recorded in 1995)
    -   Owari no Nai Yume, 8 (2010, recorded in 2008)


## Where to Buy [Redacted] {#where-to-buy-redacted}

<a id="orgb16b8b2"></a>
Redacted list of links because of continuous issues the label is having to release this album ([1](#org74d66be)), mainly on what refers to the jackets ([2](#org4bf36a9)) as noted as early as when the Q&A was made and posted above (June 2020). If you still want it, then you'll have to search it by yourself, and I recommend using a payment system that guarantees your money back if order requirements are not met.

I'm sorry for any inconveniences you may have had which were led by the links I posted before.


## Databases {#databases}

The release on databases:

-   [MusicBrainz](https://musicbrainz.org/release-group/1e2804b6-9652-41d8-b90d-d934e265b390)
-   [RateYourMusic](https://rateyourmusic.com/release/album/%E9%9D%99%E9%A6%99/%E5%A6%84%E6%83%B3%E3%81%AE%E6%A5%BD%E5%9C%92-paradise-of-delusion/)


## Etc {#etc}

-   An'archives answering a tweet concerning "Paradise of Delusion":
    <https://twitter.com/an%5Farchives/status/1268463105727758336>


## Changelog {#changelog}

2020-06-07T06:52:00+00:00
: Corrections thanks to PieNinjaProductions'
    info.
    -   Tracklist: Added No. 4 as a subitem for A4. Paradise of Delusion.
    -   Introduction: Modified spoiler to reflect addition of subitem.

2021-01-dd
: Redacted list of links in the Where to Buy section.


## References {#references}

<a id="org74d66be"></a>1.  BLACKBURN, Tommy Paquette. _Status nº 1308101249599000576_ [online]. 2020. Twitter. Available from: <https://twitter.com/3052815464/status/1308101249599000576>

<a id="org4bf36a9"></a>2.  SHANE. _Status nº 1343707826607288320_ [online]. 2020. Twitter. Available from: <https://twitter.com/1173440725/status/1343707826607288320>
